<?php

require_once '../models/tasks.php';

class Tasks_Controller extends Tasks
{ 
    public function addTaskbyData($data) {
        return $this->addTask($data);
    }

    public function editTaskbyData($data, $id) {
        return $this->editTask($data,$id);
    }

    public function deleteTaskbyID($id) {
        return $this->deleteTask($id);
    }

    public function getTasksData() {
        return $this->getTasks("all");
    }

    public function getTasksDataByProjectID($project_id) {
        return $this->getTasks($project_id);
    }

    public function getTaskData($id) {
        return $this->getTask($id);
    }

}
