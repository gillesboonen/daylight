<?php

require_once '../models/projects.php';

class Projects_Controller extends Projects
{ 
    public function addProjectbyData($data) {
        return $this->addProject($data);
    }
    
    public function editProjectbyData($data, $id) {
        return $this->editProject($data,$id);
    }

    public function deleteProjectbyID($id) {
        return $this->deleteProject($id);
    }

    public function getProjectsData() {
        return $this->getProjects();
    }

    public function getProjectData($id) {
        return $this->getProject($id);
    }

    public function getProjectHeader($id) {
        return $this->getProjectsHeader($id);
    }
}
