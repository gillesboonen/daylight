<?php

require_once '../controllers/projects_controller.php';

class Projects_View extends Projects_Controller
{ 
    public function getProjectsView() {
      $projects_array = $this->getProjectsData();
      echo "<div class=\"container\" id=\"projects_container\">
              <div class=\"page_actions\">
                <button id=\"add_project_button\" class=\"w3-button w3-green w3-medium\">Add Project</button>
              </div>
              <div id=\"projects_view\" class=\"view\">";
              foreach ($projects_array as $key => $value) {
              $this->getProjectView($value);
              };
      echo "</div></div>";
    }

    public function getProjectView($value) {
      echo "<div id=\"project_view-".$value['project_id']."\" class=\"project_view\">
              <p class=\"project_title\">".$value['project_title']."</p>
              <p class=\"project_description\">".$value['project_description']."</p>
              <p class=\"project_framework\">".$value['project_framework']."</p>
              <p class=\"project_state\">".$value['project_state']."</p>
              <p class=\"project_start_date\">since ".$value['project_start_date']."</p>
              <a class=\"project_reference\" href=\"".$value['project_reference']."\">".$value['project_reference']."</a>
              <div class=\"watermark_id\">".$value['project_id']."</div>
          </div>";
  }

    public function getTaskInfo($project_id) {
      require_once '../controllers/tasks_controller.php';
      $pc = new Tasks_Controller;
      $array = $pc->getTasksDataByProjectID($project_id);
      return $array;
    }

    public function AddProjectModal() {
      echo "
      <div id=\"add_project\">
          <div class=\"modal-content\">
            <div class=\"modal-header\">
              <span class=\"close\">&times;</span>
                <h2>Add a project</h2>
            </div>
            <div class=\"modal-body\">
              <form id=\"add_project_form\" action=\"POST\">
                <input class=\"w3-input project-input\" type=\"text\" name=\"title\" placeholder=\"Title\">
                <textarea class=\"w3-textarea project-input\" name=\"description\" id=\"\" cols=\"30\" rows=\"3\" placeholder=\"Description\"></textarea>
                <input class=\"w3-input project-input\" type=\"text\" name=\"reference\" placeholder=\"Reference\">
                <input class=\"w3-input project-input\" type=\"text\" name=\"framework\" placeholder=\"Framework\">
                <input class=\"w3-input project-input\" type=\"text\" name=\"state\" placeholder=\"State\">
                <input class=\"w3-input project-input\" type=\"text\" name=\"start_date\" placeholder=\"Date\">
                <input id=\"add_project_submit\" class=\"w3-btn w3-blue\" type=\"button\" value=\"Create project\">
              </form>
            </div>
          </div>
      </div>  ";
    }

    public function ConfirmActionWarning($action, $object, $id) {

      echo "
      <div class=\"action_confirmation\">
        <h2>Confirm ".$action." action of ".$object." ".$id."</h2>
        <p>Are you sure you want to ".$action. " ".$object." ".$id."?</p>
        <p>If no logging has been set, this action is irreversible.</p>
        <p>Please doublecheck your course of action.</p>";
        $action = strtoupper($action);
      echo "
        <button value=\"1\" class=\"w3-button w3-red w3-medium w3-margin confirm_action\"><b>$action</b></button>
        <button value=\"0\" class=\"w3-button w3-white w3-medium confirm_action\">Cancel</button>
      </div> ";
    }

    public function getProjectTaskDetails($id) {
      require_once '../controllers/tasks_controller.php';
      $tc = new Tasks_Controller;
      $task_array = $tc->getTasksDataByProjectID($id);
      return $task_array;
    }

    public function getProjectDetailsModal($id) {
      $value = $this->getProjectData($id);
      $tasks = $this->getProjectTaskDetails($id);
      echo "
      <div class=\"project_details\"  id=\"project_details-".$value['project_id']."\">
          <div class=\"modal-content\">
            <div class=\"modal-header\">
              <span class=\"close\">&times;</span>
                <h3>Details - Project ".$value['project_id']."
                  <button value=".$value['project_id']." class=\"w3-button w3-orange w3-medium w3-margin edit_project_button\">Edit Project</button>
                  <button value=".$value['project_id']." class=\"w3-button w3-red w3-medium delete_project_button\">Delete Project</button>
                </h3>
                
            </div>
            <div id=\"project_view-".$value['project_id']."\" class=\"project_view_details\">
              <p class=\"project_title\">".$value['project_title']."</p>
              <p class=\"project_description\">".$value['project_description']."</p>
              <div class=\"project_tasks\">
                <h3>Tasks</h3>
                <ul>";
                  foreach ($tasks as $key => $val) {
                    echo "<li class=\"task_list_items\" value=".$val['task_id'].">".$val['task_title']."</li>";
                  }
          echo "</ul>
              </div>
              <div class=\"project_details\">
                <p class=\"project_framework\">".$value['project_framework']."</p>
                <p class=\"project_state\">".$value['project_state']."</p>
                <p class=\"project_start_date\">since ".$value['project_start_date']."</p>
                <a class=\"project_reference\" href=\"".$value['project_reference']."\">".$value['project_reference']."</a>
              </div>
              <div class=\"watermark_id\">".$value['project_id']."</div>
            </div>
          </div>
      </div>  ";
    }

    public function getEditProjectDetailsModal($id) {
      $value = $this->getProjectData($id);
      foreach ($value as $key => $val) {
        $value[$key] = str_replace('"', "'", $val);
      }
      $tasks = $this->getProjectTaskDetails($id);
      echo "
      <div class=\"project_details\"  id=\"project_details-".$value['project_id']."\">
          <div class=\"modal-content\">
            <div class=\"modal-header\">
              <span class=\"close\">&times;</span>
                <h3>Edit Details - Project ".$id."
                <button value=".$value['project_id']." class=\"w3-button w3-green w3-medium w3-margin save_edit_project_button\">Save Edit</button>
                <button value=".$value['project_id']." class=\"w3-button w3-grey w3-medium cancel_edit_project_button\">Cancel Edit</button>
              </h3>
              
          </div>
          <form name=\"edit_project_form\" id=\"edit_project_form_".$id."\">
            <div id=\"project_view-".$id."\" class=\"project_view_details\">
              <input type=\"text\" name=\"project_title\" class=\"project_title\" placeholder=\"".$value['project_title']."\">
              <input type=\"text\" name=\"project_description\" class=\"project_description\" placeholder=\"".$value['project_description']."\">
              <div class=\"project_tasks\">
                <h3>Tasks</h3>
                <ul>";
                  foreach ($tasks as $key => $val) {
                    echo "<li class=\"task_list_items\" value=\"".$val['task_id']."\">".$val['task_title']."</li>";
                  }
          echo "</ul>
              </div>
            </form>
              <div class=\"project_details\">
              <input type=\"text\" name=\"project_framework\" class=\"project_framework\" placeholder=\"".$value['project_framework']."\">
              <input type=\"text\" name=\"project_state\" class=\"project_state\" placeholder=\"".$value['project_state']."\">
              <input type=\"text\" name=\"project_start_date\" class=\"project_start_date\" placeholder=\"".$value['project_start_date']."\">
              <input type=\"text\" name=\"project_reference\" class=\"project_reference\" placeholder=\"".$value['project_reference']."\">
            </div>
            <div class=\"watermark_id\">".$value['project_id']."</div>
          </div>
        </div>
    </div>  ";
    }


}
