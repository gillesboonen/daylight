<?php

require_once '../controllers/tasks_controller.php';

class Tasks_View extends Tasks_Controller
{ 
    public function getTasksView() {
      $tasks_array = $this->getTasksData();
      echo "<div class=\"container\" id=\"tasks_container\">
              <div class=\"page_actions\">
                <button id=\"add_task_button\" class=\"w3-button w3-green w3-medium\">Add Task</button>
              </div>
              <div id=\"tasks_view\" class=\"view\">";
              foreach ($tasks_array as $key => $value) {
              $this->getTaskView($value);
              };
      echo "</div></div>";
    }

    public function getProjectInfo($project_id) {
      require_once '../controllers/projects_controller.php';
      $pc = new Projects_Controller;
      $array = $pc->getProjectHeader($project_id);
      return $array[0];
    }

    public function getProjectsInfo() {
      require_once '../controllers/projects_controller.php';
      $pc = new Projects_Controller;
      $array = $pc->getProjectsData();
      return $array;
    }

    public function getTaskView($value) {
      $project_id = $value['project_id'];
      $header = $this->getProjectInfo($project_id);
      $project_title = $header['project_title'];
      $project_description = $header['project_description'];
      $project_state = $header['project_state'];
      echo "<div id=\"task_view-".$value['task_id']."\" class=\"task_view\">
              <p class=\"task_title\">".$value['task_title']."</p>
              <p class=\"task_description\">".$value['task_description']."</p>
              <p class=\"task_requirements\">".$value['task_requirements']."</p>
              <p class=\"task_status\">".$value['task_status']."</p>
              <div class=\"watermark_id\">".$value['task_id']."</div>
              <div class=\"watermark_project\">".$project_title."</div>
          </div>";
  }

    public function AddTaskModal() {
      $projects = $this->getProjectsInfo();
      echo "
      <div id=\"add_task\">
          <div class=\"modal-content\">
            <div class=\"modal-header\">
              <span class=\"close\">&times;</span>
                <h2>Add a task</h2>
            </div>
            <div class=\"modal-body\">
              <form id=\"add_task_form\" action=\"POST\">
                <input class=\"w3-input w3-margin task-input\" type=\"text\" name=\"title\" placeholder=\"Title\">
                <textarea class=\"w3-textarea w3-margin task-input\" name=\"description\" id=\"\" cols=\"30\" rows=\"3\" placeholder=\"Description\"></textarea>
                <select id=\"select_projects\" name=\"project_id\" class=\"w3-margin\">";
                 foreach ($projects as $key => $value) {
                  $project_id = $value['project_id'];
                  $project_title = $value['project_title'];
                   echo "<option value=".$project_id.">".$project_id." - " .$project_title."</option>";
                 }
                 echo "
                </select> 
                <input class=\"w3-input w3-margin task-input\" type=\"text\" name=\"requirements\" placeholder=\"Requirements\">
                <input class=\"w3-input w3-margin task-input\" type=\"text\" name=\"status\" placeholder=\"Status\">
                <input id=\"add_task_submit\" class=\"w3-btn w3-blue\" w3-margin type=\"button\" value=\"Create task\">
              </form>
            </div>
          </div>
      </div>  ";
    }

    public function ConfirmActionWarning($action, $object, $id) {

      echo "
      <div class=\"action_confirmation\">
        <h2>Confirm ".$action." action of ".$object." ".$id."</h2>
        <p>Are you sure you want to ".$action. " ".$object." ".$id."?</p>
        <p>If no logging has been set, this action is irreversible.</p>
        <p>Please doublecheck your course of action.</p>";
        $action = strtoupper($action);
      echo "
        <button value=\"1\" class=\"w3-button w3-red w3-medium w3-margin confirm_action\"><b>$action</b></button>
        <button value=\"0\" class=\"w3-button w3-white w3-medium confirm_action\">Cancel</button>
      </div> ";
    }

    public function getTaskDetailsModal($id) {
      $value = $this->getTaskData($id);
      $project_id = $value['project_id'];

      $header = $this->getProjectInfo($project_id);
      $project_title = $header['project_title'];
      $project_description = $header['project_description'];
      $project_state = $header['project_state'];

      echo "
      <div class=\"task_details\"  id=\"task_details-".$value['task_id']."\">
          <div class=\"modal-content\">
            <div class=\"modal-header\">
              <span class=\"close\">&times;</span>
                <h3>Details - Task ".$value['task_id']."
                  <button value=".$value['task_id']." class=\"w3-button w3-orange w3-medium w3-margin edit_task_button\">Edit Task</button>
                  <button value=".$value['task_id']." class=\"w3-button w3-red w3-medium delete_task_button\">Delete Task</button>
                </h3>
                
            </div>
            <div id=\"task_view-".$value['task_id']."\" class=\"task_view_details\">
              <p class=\"task_title\">".$value['task_title']."</p>
              <p class=\"task_description\">".$value['task_description']."</p>
              <p value=".$project_id." class=\"task_project_id project_links\">relates to project #".$value['project_id']."</p>
              <p value=".$project_id." class=\"task_project_title project_links\">Project ".$project_title."</p>
              <p class=\"task_project_description\">".$project_description."</p>
              <p class=\"task_project_state\">".$project_state."</p>
              <p class=\"task_requirements\">Requirements - ".$value['task_requirements']."</p>
              <p class=\"task_status\">Task status - ".$value['task_status']."</p>
              <div class=\"watermark_id\">".$value['task_id']."</div>
              <div class=\"watermark_project\">".$project_title."</div>
            </div>
          </div>
      </div>  ";
    }

    public function getEditTaskDetailsModal($id) {
      $value = $this->getTaskData($id);
      foreach ($value as $key => $val) {
        $value[$key] = str_replace('"', "'", $val);
      }
      $project_id = $value['project_id'];    

      $header = $this->getProjectInfo($project_id);
      $project_title = htmlspecialchars($header['project_title'], ENT_QUOTES);
      $project_description = htmlspecialchars($header['project_description'], ENT_QUOTES);
      $project_state = htmlspecialchars($header['project_state'], ENT_QUOTES);

      echo "
      <div class=\"task_details\"  id=\"task_details-".$value['task_id']."\">
          <div class=\"modal-content\">
            <div class=\"modal-header\">
              <span class=\"close\">&times;</span>
                <h3>Edit Details - Task ".$value['task_id']."
                  <button value=".$value['task_id']." class=\"w3-button w3-green w3-medium w3-margin save_edit_task_button\">Save Edit</button>
                  <button value=".$value['task_id']." class=\"w3-button w3-grey w3-medium cancel_edit_task_button\">Cancel Edit</button>
                </h3>                
                </div>
              <form name=\"edit_task_form\" id=\"edit_task_form_".$value['task_id']."\">
                <div id=\"task_view-".$value['task_id']."\" class=\"task_view_details\">
                  <input type=\"text\" name=\"task_title\" class=\"task_title\" placeholder=\"".$value['task_title']."\"><br>
                  <input type=\"text\" name=\"task_description\" class=\"task_description\" placeholder=\"".$value['task_description']."\"><br>
                  <input type=\"text\" name=\"task_project_id\" class=\"task_project_id\" placeholder=\"Project ID: ".$project_id."\">
                  <p name=\"task_project_title\" class=\"task_project_title\">".$project_title."</p>
                  <p name=\"task_project_description\" class=\"task_project_description\">".$project_description."</p>
                  <p name=\"task_project_state\" class=\"task_project_state\">".$project_state."</p><br>
                  <input type=\"text\" name=\"task_requirements\" class=\"task_requirements\" placeholder=\"".$value['task_requirements']."\">
                  <p name=\"task_status\" class=\"task_status\">".$value['task_status']."</p>
                  <div name=\"task_id\" value=\"task_id\" class=\"watermark_id\">".$value['task_id']."</div>
                  <div class=\"watermark_project\">".$project_title."</div>
                </div>
              </form>
          </div>
      </div>  ";
    }

}
