<?php

require_once '../controllers/main_controller.php';

class Main_View extends Main_Controller
{ 
    public function getMainView() {
      echo "<div class=\"container\" id=\"main_container\">
              <div class=\"page_actions\">
                <button id=\"add_project_button\" class=\"w3-button w3-green w3-medium\">Add Project</button>
              </div>
              <div id=\"main_view\" class=\"view\">
              <h1>Welcome, Gilles</h1>
              </div>
            </div>";
    }


}
