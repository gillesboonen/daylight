<?php

require_once '../views/projects_view.php';

$ap = new Projects_View;
$id = $_POST['id'];
return $ap->getEditProjectDetailsModal($id);
