<?php

$object = $_POST['object'];

if ($object == "project") {
    require_once '../views/projects_view.php';
    $ca = new Projects_View;

} else if ($object == "task") {
    require_once '../views/tasks_view.php';
    $ca = new Tasks_View;
}


$action = $_POST['action'];
$id = $_POST['id'];
$result = $ca->ConfirmActionWarning($action, $object, $id);
return $result;
