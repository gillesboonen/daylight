<?php

require_once '../views/tasks_view.php';

$ap = new Tasks_View;
$id = $_POST['id'];
return $ap->getEditTaskDetailsModal($id);
