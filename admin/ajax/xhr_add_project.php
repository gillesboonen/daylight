<?php

require_once '../controllers/projects_controller.php';

$ap = new Projects_Controller;
$data = [];
$data['title'] = $_POST['title'];
$data['description'] = $_POST['description'];
$data['reference'] = $_POST['reference'];
$data['framework'] = $_POST['framework'];
$data['image'] = "";
$data['state'] = $_POST['state'];
$data['start_date'] = $_POST['start_date'];
$ap->addProjectbyData($data);
