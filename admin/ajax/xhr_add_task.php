<?php

require_once '../controllers/tasks_controller.php';

$ap = new Tasks_Controller;
$data = [];
$data['title'] = $_POST['title'];
$data['description'] = $_POST['description'];
$data['project_id'] = $_POST['project_id'];
$data['requirements'] = $_POST['requirements'];
$data['status'] = $_POST['status'];
$ap->addTaskbyData($data);
