

  function xhrGetMain() {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_main.php',
      success: function(data) {
        $('#root').html(data);
      }
    });
  }

  function xhrGetProjects() {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_projects.php',
      success: function(data) {
        $('#root').html(data);
      }
    });
  }

  function xhrGetTasks() {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_tasks.php',
      success: function(data) {
        $('#root').html(data);
      }
    });
  }

  function xhrConfirmAction($action, $object, $id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_confirm_action.php',
      data: {action:$action, object:$object, id:$id},
      success: function(data) {
        return data;
      }
    });

  }

  function xhrDeleteTask($id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_delete_task.php',
      data: {id:$id},
      success: function() {
        $('#modal').css("display", "none");
        $('#modal').html("");
      },
      complete: function () {
        xhrGetTasks().done(function(){
          console.log("Get tasks done");
      });
      }
    });
  }

  function xhrDeleteProject($id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_delete_project.php',
      data: {id:$id},
      success: function() {
        $('#modal').css("display", "none");
        $('#modal').html("");
      },
      complete: function () {
        xhrGetProjects().done(function(){
          console.log("Get projects done");
      });
      }
    });
  }

  function xhrGetProject($id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_project_details.php',
      data: {id:$id},
      success: function(data) {
        $('#modal').html(data);
      }
    });
  }

  function xhrGetTask($id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_task_details.php',
      data: {id:$id},
      success: function(data) {
        $('#modal').html(data);
      }
    });
  }

  function xhrGetEditProject($id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_edit_project_details.php',
      data: {id:$id},
      success: function(data) {
        $('#modal').html(data);
      }
    });
  }

  function xhrGetEditTask($id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_edit_task_details.php',
      data: {id:$id},
      success: function(data) {
        $('#modal').html(data);
      }
    });
  }

  function xhrSaveEditProject($form_data, $project_id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_save_edit_project_details.php',
      data: {data:$form_data, id:$project_id}
    });
  }


  function xhrSaveEditTask($form_data, $task_id) {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_save_edit_task_details.php',
      data: {data:$form_data, id:$task_id}
    });
  }

  function xhrAddProject() {
    return $.ajax({
      url:'../ajax/xhr_add_project.php',
      type:'post',
      data:$('#add_project_form').serialize(),
      success:function(data){
        $('#modal').css("display", "none");
        $('#modal').html("");
        console.log("Project #" +data+ " added.");
      },
      complete: function () {
        xhrGetProjects().done(function(){
          console.log("Get projects done");
      });
       }
  });
  }

  function xhrAddProjectModal() {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_add_project_modal.php',
      success: function(data) {
        $('#modal').html(data);
      }
    });
  }

  function xhrAddTask() {
    return $.ajax({
      url:'../ajax/xhr_add_task.php',
      type:'post',
      data:$('#add_task_form').serialize(),
      success:function(data){
        $('#modal').css("display", "none");
        $('#modal').html("");
        console.log("Task #" +data+ " added.");
      },
      complete: function () {
        xhrGetTasks().done(function(){
          console.log("Get tasks done");
      });
       }
  });
  }

  function xhrAddTaskModal() {
    return $.ajax({
      type: 'POST',
      url: '../ajax/xhr_get_add_task_modal.php',
      success: function(data) {
        $('#modal').html(data);
      }
    });
  }

$(document).ready(function() { 
  var activePage;

  xhrGetMain().done(function(){
    console.log("Get intro done");
  });
  activePage = "main";

  $("#nav_home").on("click", function(){
    $(".nav_buttons").removeClass("active");
    xhrGetMain().done(function(){ 
      console.log("Get main done");
      $(".nav_links li a").animate({opacity:1}, 1000);  
      $("#nav_home").animate({opacity:0});  
    });
    activePage = "main";
  }); 

  $( ".nav_buttons" ).each(function() {
    $(this).on("click", function(){
      $(".nav_buttons").removeClass("active");
        var pageName = $(this).attr('name');
        if (pageName == "projects") {
          xhrGetProjects().done(function(){
            console.log("Get projects done");
            $("#projects_container").animate({opacity:1}, 2000);  
            $(".nav_links li a").animate({opacity:1}, 1000);  
            $("#projects_nav").animate({opacity:0}, 1000); 
        });
        $(this).addClass("active");
        } else if (pageName == "tasks") {
          xhrGetTasks().done(function(){
            console.log("Get tasks done");
            $("#tasks_container").animate({opacity:1}, 2000);  
            $(".nav_links li a").animate({opacity:1}, 1000);  
            $("#tasks_nav").animate({opacity:0}, 1000); 
        });
        $(this).addClass("active");
        }
        activePage = pageName;

        if (activePage !== "main") {
          console.log("is not main");
          $("#nav_home").animate({opacity:1}, 2000);  
        } else {
          $("#nav_home").animate({opacity:0}, 2000);  
        }
    });
  });

  $(document).on('click',".project_view",function() {
      var project_id = $(this).attr('id').split('-');
      project_id = project_id[1];
      xhrGetProject(project_id).done(function(){});
      $('#modal').css("display", "flex");
      $(document).on('click',".delete_project_button",function() {
        xhrConfirmAction("delete", "project", project_id).done(function(data){
          $('#warning').css("display", "flex");
          $('#warning').html(data);
        });
        $(document).on('click',".confirm_action",function() {
          $confirm = $(this).attr('value');
          if ($confirm == 1) {
            xhrDeleteProject(project_id).done(function(){
              console.log("Project #" +project_id+ " deleted.");
              $('#warning').css("display", "none");
              $('#warning').html("");});
          } else {
            console.log("Project #" +project_id+ " was not deleted.");
            $('#warning').css("display", "none");
            $('#warning').html("");
          }
        });
    });

  });

  $(document).on('click',".task_view",function() {
    var task_id = $(this).attr('id').split('-');
    task_id = task_id[1];
    xhrGetTask(task_id).done(function(){});
    $('#modal').css("display", "flex");
    $(document).on('click',".delete_task_button",function() {
      xhrConfirmAction("delete", "task", task_id).done(function(data){
        $('#warning').css("display", "flex");
        $('#warning').html(data);
      });
      $(document).on('click',".confirm_action",function() {
        $confirm = $(this).attr('value');
        if ($confirm == 1) {
          xhrDeleteTask(task_id).done(function(){
            console.log("Task #" +task_id+ " deleted.");
            $('#warning').css("display", "none");
            $('#warning').html("");});
        } else {
          console.log("Task #" +task_id+ " was not deleted.");
          $('#warning').css("display", "none");
          $('#warning').html("");
        }
      });

    });

  });

  $(document).on('click',".edit_project_button",function() {
    $project_id = $(this).attr('value');
    xhrGetEditProject($project_id).done(function(){});
  });

  $(document).on('click',".save_edit_project_button",function() {
    $form_data = {};
    $("#edit_project_form_"+$project_id).serializeArray().map(function(x){$form_data[x.name] = x.value;});
    xhrSaveEditProject($form_data, $project_id).done(function() {});      
    xhrGetProject($project_id).done(function(){});
  });

  $(document).on('click',".cancel_edit_project_button",function() {  
    xhrGetProject($project_id).done(function(){});
  });

  $(document).on('click',".edit_task_button",function() {
    $task_id = $(this).attr('value');
    xhrGetEditTask($task_id).done(function(){});

  });

  $(document).on('click',".save_edit_task_button",function() {
    $task_id = $(this).attr('value');
    $form_data = {};
    $("#edit_task_form_"+$task_id).serializeArray().map(function(x){$form_data[x.name] = x.value;});
    xhrSaveEditTask($form_data, $task_id).done(function() {});      
    xhrGetTask($task_id).done(function(){});
  });

  $(document).on('click',".cancel_edit_task_button",function() {  
    $task_id = $(this).attr('value');
    xhrGetTask($task_id).done(function(){});
  });

  $(document).on('click',".project_links",function() {
    $id = $(this).attr('value');
    console.log("clicked on : " + $id);
    xhrGetProject($id).done(function(){});
  });

  $(document).on('click',".task_list_items",function() {
    $id = $(this).attr('value');
    console.log("clicked on : " + $id);
    xhrGetTask($id).done(function(){});
  });
  
});

$(window).on('load', function () {
  $(document).on('click',"button#add_project_button",function() {
    xhrAddProjectModal().done(function(){});
    $('#modal').css("display", "flex");
  });

  $(document).on('click',"button#add_task_button",function() {
    xhrAddTaskModal().done(function(){});
    $('#modal').css("display", "flex");
  });

  $(document).on('click',"span.close",function() {
    $('#modal').css("display", "none");
    $('#modal').html("");
  });

  $(document).on('click',"#add_project_submit",function() {
    xhrAddProject().done(function(){});
  });
  
  $(document).on('click',"#add_task_submit",function() {
    xhrAddTask().done(function(){});
  });

});