<?php

require_once 'db.php';

class Projects 
{

    public function addProject($data) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];

        // Check connection
        // If connection has error
        if ($conn->connect_error) {
            // Return a fail value to the result key of the dbc array
            $ar['dbc']['result'] = 0;
            // Return the error message as value of the error key of the dbc array
            $ar['dbc']['error'] = $conn->connect_error;
        }

        // Initialize statement via db connection
        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $title = $data['title'];
        $description = $data['description'];
        $reference = $data['reference'];
        $framework = $data['framework'];
        $state = $data['state'];
        $images = $data['images'];
        $start_date = $data['start_date'];
    
        $stmts["add_project"] = "INSERT INTO projects (project_title, project_description, project_reference, project_framework, project_state, project_image, project_start_date) VALUES (?, ?, ?, ?, ?, ?, ?)";

        // Prepare SQL statements

        if (!mysqli_stmt_prepare($stmt, $stmts["add_project"])) {
            echo "helloo fail prep";
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            return $ar['dbc']['error'];
        } else {
            $stmt->bind_param("sssssss", $title, $description, $reference, $framework, $state, $images, $start_date);
            // Execute statement
            if($stmt->execute()) {
                $ar['dbc']['result'] = 1;
                $ar['result'] = 1;
                $ar['project_id'] = $conn->insert_id;
            } else {
                echo "hello execute fail";
                $ar['result'] = $stmt->error;
                echo $ar['result'];
            }
            $stmt->close();
            $conn->close();
            printf(json_encode($ar['project_id']));
        }
    }

    public function editProject($data, $id) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];
        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $title = $data['project_title'];
        $description = $data['project_description'];
        $framework = $data['project_framework'];
        $state = $data['project_state'];
        $start_date = $data['project_start_date'];
        $reference = $data['project_reference'];


        $project_id = $id;
        $project = $this->getProject($project_id);
        $former_title = $project['project_title'];
        $former_description = $project['project_description'];
        
        $former_framework = $project['project_framework'];
        $former_state = $project['project_state'];
        $former_start_date = $project['project_start_date'];
        $former_reference = $project['project_reference'];

        if ($title == "") {
            $title = $former_title;
        } 
        if ($description == "") {
            $description = $former_description;
        }
        if ($framework == "") {
            $framework = $former_framework;
        } 
        if ($state == "") {
            $state = $former_state;
        }
        if ($start_date == "") {
            $start_date = $former_start_date;
        } 
        if ($reference == "") {
            $reference = $former_reference;
        }
        
        $stmts["edit_task"] = "UPDATE projects SET project_title=?, project_description=?, project_framework=?, project_state=?, project_reference=?, project_start_date=? WHERE project_id = ?";   

        if (!mysqli_stmt_prepare($stmt, $stmts["edit_task"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            echo "error prepare";
        } else {
            $stmt->bind_param("ssssssi", $title, $description, $framework, $state, $reference, $start_date, $project_id);
            if($stmt->execute()) {
                $ar['dbc']['result'] = 1;
                $ar['result'] = 1;
            } else {
                $ar['result'] = $stmt->error;
                echo "error execute";
            }
            $stmt->close();
            $conn->close();
            return $project_id;
        }
    }

    public function getProject($id) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];

        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];


        $stmts["get_project"] = "SELECT * FROM projects WHERE project_id = ?";

        if (!mysqli_stmt_prepare($stmt, $stmts["get_project"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            return;
        } else {
                $stmt->bind_param("i", $id);

            if($stmt->execute()) {
                $result = $stmt->get_result();
                $row = $result->fetch_assoc();
                $ar['result'] = $row;    
                $ar['dbc']['result'] = 1;
            } else {
                $ar['result']['error'] = $stmt->error;
            }
            $stmt->close();
            $conn->close();
        }
        return $ar['result'];
    }

    public function getProjects() {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];

        // Check connection
        // If connection has error
        if ($conn->connect_error) {
            // Return a fail value to the result key of the dbc array
            $ar['dbc']['result'] = 0;
            // Return the error message as value of the error key of the dbc array
            $ar['dbc']['error'] = $conn->connect_error;
        }

        // Initialize statement via db connection
        $stmt = mysqli_stmt_init($conn);
        $stmts = [];


        $stmts["get_projects"] = "SELECT * FROM projects ORDER BY project_id ASC";

        // Prepare SQL statements

        if (!mysqli_stmt_prepare($stmt, $stmts["get_projects"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            return;
        } else {
            if($stmt->execute()) {
                $ar['result']['projects'] = [];
                $result = $stmt->get_result();
                while ($row = $result->fetch_assoc()) {
                    $ar['result']['projects'][] = $row;
                }
                $ar['dbc']['result'] = 1;
            } else {
                $ar['result']['error'] = $stmt->error;
            }
            $stmt->close();
            $conn->close();
        }
        return $ar['result']['projects'];
    }

    public function getProjectsHeader($project_id) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];
        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];


        $stmts["get_projects"] = "SELECT project_id, project_title, project_description, project_state FROM projects WHERE project_id = ? ORDER BY project_id ASC";

        if (!mysqli_stmt_prepare($stmt, $stmts["get_projects"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            return;
        } else {
            $stmt->bind_param("i", $project_id);
            if($stmt->execute()) {
                $ar['result']['projects'] = [];
                $result = $stmt->get_result();
                while ($row = $result->fetch_assoc()) {
                    $ar['result']['projects'][] = $row;
                }
                $ar['dbc']['result'] = 1;
            } else {
                $ar['result']['error'] = $stmt->error;
            }
            $stmt->close();
            $conn->close();
        }
        return $ar['result']['projects'];
    }

    public function deleteProject($id) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];
        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }
      $stmt = mysqli_stmt_init($conn);
      $stmts = [];

      $stmts["delete_project"] = "DELETE FROM projects WHERE project_id = ?";

      if (!mysqli_stmt_prepare($stmt, $stmts["delete_project"])) {
          $ar['dbc']['result'] = 0;
          $ar['dbc']['error'] = $stmt->error;
          echo $ar['dbc']['error'];
      } else {
            $stmt->bind_param("i", $id);
            if($stmt->execute()) {
                $ar['result'] = 1;
            } else {
                $ar['result'] = $stmt->error;
            }
            $stmt->close();
            $conn->close();
        }
        printf(json_encode($id));
    }
    
}
