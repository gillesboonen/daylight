<?php

require_once 'db.php';

class Tasks 
{

    public function addTask($data) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];
        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $title = $data['title'];
        $description = $data['description'];
        $requirements = $data['requirements'];
        $status = $data['status'];
        $project_id = $data['project_id'];
    
        $stmts["add_task"] = "INSERT INTO tasks (task_title, task_description, project_id, task_requirements, task_status) VALUES (?, ?, ?, ?, ?)";

        if (!mysqli_stmt_prepare($stmt, $stmts["add_task"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            return $ar['dbc']['error'];
        } else {
            $stmt->bind_param("ssiss", $title, $description, $project_id, $requirements, $status);
            if($stmt->execute()) {
                $ar['dbc']['result'] = 1;
                $ar['result'] = 1;
                $ar['task_id'] = $conn->insert_id;
            } else {
                $ar['result'] = $stmt->error;
            }
            $stmt->close();
            $conn->close();
            printf(json_encode($ar['task_id']));
        }
    }

    public function editTask($data, $id) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];
        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $title = $data['task_title'];
        $description = $data['task_description'];
        $requirements = $data['task_requirements'];
        $project_id = $data['task_project_id'];

        $task_id = $id;
        $task = $this->getTask($task_id);
        $task_title_former = $task['task_title'];
        $task_description_former = $task['task_description'];
        $task_requirements_former = $task['task_requirements'];
        $project_id_former = $task['project_id'];

        if ($title == "") {
            $title = $task_title_former;
        } 
        if ($description == "") {
            $description = $task_description_former;
        }
        if ($requirements == "") {
            $requirements = $task_requirements_former;
        } 
        if ($project_id == "") {
            $project_id = $project_id_former;
        }
    
        $stmts["edit_task"] = "UPDATE tasks SET task_title=?, task_description=?, project_id=?, task_requirements=? WHERE task_id = ?";   

        if (!mysqli_stmt_prepare($stmt, $stmts["edit_task"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            echo "error prepare";
        } else {
            $stmt->bind_param("ssisi", $title, $description, $project_id, $requirements, $task_id);
            if($stmt->execute()) {
                $ar['dbc']['result'] = 1;
                $ar['result'] = 1;
            } else {
                $ar['result'] = $stmt->error;
                echo "error execute";
            }
            $stmt->close();
            $conn->close();
            return $task_id;
        }
    }

    public function getTasks($value) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];

        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        if ($value == "all") {
            $stmts["get_tasks"] = "SELECT * FROM tasks ORDER BY task_id ASC";
        } else {
            $stmts["get_tasks"] = "SELECT * FROM tasks WHERE project_id = ? ORDER BY task_id ASC";
        }


        if (!mysqli_stmt_prepare($stmt, $stmts["get_tasks"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            return;
        } else {
            if ($value !== "all") {
                $stmt->bind_param("i", $value);                
            }
            if($stmt->execute()) {
                $ar['result']['tasks'] = [];
                $result = $stmt->get_result();
                while ($row = $result->fetch_assoc()) {
                    $ar['result']['tasks'][] = $row;
                }
                $ar['dbc']['result'] = 1;
            } else {
                $ar['result']['error'] = $stmt->error;
            }
            $stmt->close();
            $conn->close();
        }
        return $ar['result']['tasks'];
    }

    public function getTask($task_id) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];

        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $stmts["get_task"] = "SELECT * FROM tasks WHERE task_id = ?";

        if (!mysqli_stmt_prepare($stmt, $stmts["get_task"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            return;
        } else {
                $stmt->bind_param("i", $task_id);

            if($stmt->execute()) {
                $result = $stmt->get_result();
                $row = $result->fetch_assoc();
                $ar['result'] = $row;    
                $ar['dbc']['result'] = 1;
            } else {
                $ar['result']['error'] = $stmt->error;
            }
            $stmt->close();
            $conn->close();
        }
        return $ar['result'];
    }

    public function deleteTask($task_id) {
        $be = new dataBase;
        $db = $be->getHomelabCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db['name']);
        mysqli_set_charset($conn, 'utf8');
        $ar = [];
        $ar['dbc'] = [];
        $ar['result'] = [];
        if ($conn->connect_error) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $conn->connect_error;
        }
      $stmt = mysqli_stmt_init($conn);
      $stmts = [];

      $stmts["delete_task"] = "DELETE FROM tasks WHERE task_id = ?";

      if (!mysqli_stmt_prepare($stmt, $stmts["delete_task"])) {
          $ar['dbc']['result'] = 0;
          $ar['dbc']['error'] = $stmt->error;
      } else {
            $stmt->bind_param("i", $task_id);
            if($stmt->execute()) {
                $ar['result'] = 1;
            } else {
                $ar['result'] = $stmt->error;
            }
            $stmt->close();
            $conn->close();
        }
        printf(json_encode($task_id));
    }
    
}
